[![build status](https://gitlab.com/ericpoe/AOC2016/badges/master/build.svg)](https://gitlab.com/ericpoe/AOC2016/commits/master)
[![coverage report](https://gitlab.com/ericpoe/AOC2016/badges/master/coverage.svg)](https://gitlab.com/ericpoe/AOC2016/commits/master)
# AOC2016
2016 Advent of Code

These are my submissions to the [2016 Advent of Code](http://adventofcode.com/).

Languages used:

* PHP 7.1