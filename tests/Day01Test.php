<?php
namespace Aoc2016\Tests;

use Aoc2016\Day01\Traveler;

class Day01Test extends \PHPUnit_Framework_TestCase
{
    /** @var Traveler */
    private $traveler;

    public function setUp()
    {
        $this->traveler = new Traveler();
    }

    public function testNoMovementIsDistanceZero()
    {
        $this->assertEquals(0, $this->traveler->getDistance());
    }

    /**
     * @dataProvider directionStringProvider
     */
    public function testTokenizeString(string $input, array $expected)
    {
        $this->traveler->read($input);

        $this->assertSame($expected, $this->traveler->getTokens());
    }

    public function directionStringProvider()
    {
        return [
            "One step" => [
                "R2",
                [
                    ["R", 2]
                ]
            ],
            "Two steps" => [
                "R2, L3",
                [
                    ["R", 2],
                    ["L", 3]
                ]
            ],
            "Four steps with a long number thrown in" => [
                "R5, L5, R545, R3",
                [
                    ["R", 5],
                    ["L", 5],
                    ["R", 545],
                    ["R", 3]
                ]
            ],
        ];
    }

    /**
     * @dataProvider finalCoordinatesProvider
     */
    public function testDetermineFinalCoordinates(string $input, array $expected)
    {
        $this->traveler->read($input);

        $this->assertSame($expected, $this->traveler->getFinalCoordinates());
    }

    public function finalCoordinatesProvider()
    {
        return [
            "One step" => [
                "R2",
                [2, 0]
            ],
            "Two steps" => [
                "R2, L3",
                [2, 3]
            ],
            "Three steps" => [
                "R2, R2, R2",
                [0, -2]
            ],
            "Four steps" => [
                "R5, L5, R5, R3",
                [10, 2]
            ],
        ];
    }

    /**
     * @dataProvider finalDistanceProvider
     */
    public function testFinalDistance(string $input, int $expected)
    {
        $this->traveler->read($input);

        $this->assertSame($expected, $this->traveler->getDistance());
    }

    public function finalDistanceProvider()
    {
        /**
         * Columns: [string, final distance]
         */
        return [
            "One step" => [
                "R2",
                2
            ],
            "Two steps" => [
                "R2, L3",
                5
            ],
            "Three steps" => [
                "R2, R2, R2",
                2
            ],
            "Four steps" => [
                "R5, L5, R5, R3",
                12
            ],
            "Four Steps In A Square" => [
                "R2, R2, R2, R2",
                0
            ]
        ];
    }

    /**
     * @dataProvider firstCollisionProvider
     *
     * @param string $input
     * @param int    $expected
     */
    public function testDistanceFromFirstCollision(string $input, int $expected)
    {
        $this->traveler->endAtFirstIntersection();
        $this->traveler->read($input);

        $this->assertEquals($expected, $this->traveler->getDistance());
    }

    public function firstCollisionProvider()
    {
        return [
            "Travel in a perfect rectangle" => [
                "R8, R4, R4, R8",
                4
            ],
            "Tiny Step Collision" => [
                "R1, L2, R1, R1, R2",
                2
            ],
            "Still tiny step collision" => [
                "R2, R3, R2, R1, R4",
                4
            ],
        ];
    }
}
