<?php
namespace Aoc2016\Tests;

use Aoc2016\Day03\TriangleThingy;

class Day03Test extends \PHPUnit_Framework_TestCase
{
    /** @var  TriangleThingy*/
    protected $triangleThingy;

    public function setUp()
    {
        $this->triangleThingy = new TriangleThingy();
    }

    public function testFindMax()
    {
        $numberString = "5 10 25";

        $this->assertSame("25", $this->triangleThingy->getMax($numberString));
    }

    public function testFindOthers()
    {
        $numberString = "5 10 25";

        $this->assertSame(["5", "10"], $this->triangleThingy->getMinAndMiddle($numberString));
    }

    public function testIfTriangleCandidate()
    {
        $numberString = "5 10 25";
        $this->assertFalse($this->triangleThingy->isTriangleCandidate($numberString));
    }

    public function testCanReadInput()
    {
        $this->triangleThingy->read();
        $this->assertArrayHasKey(5, $this->triangleThingy->getRawTriangleCandidates());
    }

    public function testCanFilterCandidates()
    {
        $this->triangleThingy->read();
        $filteredCandidates = $this->triangleThingy->getFilteredCandidates();

        $this->assertSame(862, count($filteredCandidates));
    }
}
