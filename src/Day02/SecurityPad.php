<?php
declare (strict_types=1);

namespace Aoc2016\Day02;

use Haystack\HArray;
use Haystack\HString;

class SecurityPad
{
    /** @var array */
    protected $coord;

    /** @var HArray */
    protected $instructions;

    /** @var string */
    protected $value;

    /** @var array */
    protected $dimensions;

    protected $grid;

    public function __construct()
    {
        $this->coord = [1, 1];
        $this->instructions = new HArray();
        $this->value = "";
        $this->setDimensions(3, 3);
        $this->grid = $this->buildGrid();
    }

    public function setDimensions(int $width, int $height) : void
    {
        $this->dimensions = ["width" => $width, "height" => $height];
    }

    public function setCoordinates(int $x = 1, int $y = 1) : void
    {
        if ($x < 0) $x = 0;
        if ($y < 0) $y = 0;

        if (isset($this->dimensions)) {
            if ($x > $this->dimensions["width"] - 1) $x = $this->dimensions["width"] - 1;
            if ($y > $this->dimensions["height"] - 1) $y = $this->dimensions["height"] - 1;
        }

        $this->coord = [$x, $y];
    }

    public function getCoordinates() : array
    {
        return $this->coord;
    }

    public function getValue(array $coord = []) : string
    {
        return empty($coord) ? $this->value : (string) array_search($coord, $this->grid);
    }

    public function buildGrid() : array
    {
        $arr = [];
        $idx = 1;

        for ($i = $this->dimensions["height"] - 1; $i >= 0; $i--) {
            for ($j = 0; $j < $this->dimensions["width"]; $j++) {
                $arr[$idx++] = [$j, $i];
            }
        }

        return $arr;
    }

    public function read(string $line) : void
    {
        $hLine = new HString($line);
        $this->instructions = $hLine->toHArray("\n");
    }

    public function run() : void
    {
        foreach ($this->instructions as $line) {
            $this->runLineOfInstructions(new HString($line));
        }
    }

    protected function runLineOfInstructions(HString $hLine) : void
    {
        foreach ($hLine as $step) {
            $this->travel($step);
        }

        $this->value .= array_search($this->coord, $this->grid);
    }

    protected function travel(string $dir) : void
    {
        [$x, $y] = $this->coord;

        switch ($dir) {
            case 'U':
                $y++;
                break;
            case 'D':
                $y--;
                break;
            case 'R':
                $x++;
                break;
            default:
                $x--;
        }

        $this->setCoordinates($x, $y);
    }
}
