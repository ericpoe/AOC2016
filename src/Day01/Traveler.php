<?php
declare (strict_types = 1);
namespace Aoc2016\Day01;

class Traveler
{
    /** @var array Final Coordinates */
    protected $coordFinal;

    /**
     * Columns are X, Y
     * @var array
     */
    protected $intersections;

    /** @var int */
    protected $distance;

    /** @var array */
    protected $tokens;

    /** @var string */
    protected $cardinalDirection;

    /** @var bool */
    protected $isCollisionFree;

    /** @var bool */
    protected $checkForIntersections;

    public function __construct()
    {
        $this->coordFinal = [0, 0];
        $this->distance = 0;
        $this->tokens = [];
        $this->cardinalDirection = 'N';
        $this->intersections = [[0, 0]];
        $this->isCollisionFree = true;
        $this->checkForIntersections = false;
    }

    public function getDistance() : int
    {
        return $this->distance;
    }

    protected function calculateDistance() : void
    {
        $this->distance = abs($this->coordFinal[0]) + abs($this->coordFinal[1]);
    }

    public function getTokens() : array
    {
        return $this->tokens;
    }

    public function endAtFirstIntersection() : void
    {
        $this->checkForIntersections = true;
    }

    public function read(string $instructions) : void
    {
        $directions = explode(',', $instructions);

        $tokens = array_map(function ($direction) {
            return trim($direction);
        }, $directions);

        $this->tokens = array_map(function ($token) {
            return [$token[0], intval(substr($token, 1))];
        }, $tokens);

        $this->calculateFinalCoordinates();
        $this->calculateDistance();
    }

    public function getFinalCoordinates() : array
    {
        return $this->coordFinal;
    }

    protected function calculateFinalCoordinates() : void
    {
        foreach ($this->tokens as $instruction) {
            if ('R' == $instruction[0]) {
                $this->goRight($instruction[1]);
            } else {
                $this->goLeft($instruction[1]);
            }
        }
    }

    protected function goRight(int $blocksToTravel) : void
    {
        switch ($this->cardinalDirection) {
            case 'N':
                $this->cardinalDirection = 'E';
                break;
            case 'E':
                $this->cardinalDirection = "S";
                break;
            case 'S':
                $this->cardinalDirection = 'W';
                break;
            default:
                $this->cardinalDirection = 'N';
        }
        $this->travelAlongAnAxis($blocksToTravel);
    }

    protected function goLeft(int $blocksToTravel) : void
    {
        switch ($this->cardinalDirection) {
            case 'N':
                $this->cardinalDirection = 'W';
                break;
            case 'W':
                $this->cardinalDirection = "S";
                break;
            case 'S':
                $this->cardinalDirection = 'E';
                break;
            default:
                $this->cardinalDirection = 'N';
        }
        $this->travelAlongAnAxis($blocksToTravel);
    }

    protected function travelAlongAnAxis(int $blocksToTravel) : void
    {
        if (!$this->isCollisionFree) return;

        if ($this->checkForIntersections) {
            $this->travelByIntersection($blocksToTravel);
            return;
        }

        switch ($this->cardinalDirection) {
            case 'N':
                $this->coordFinal[1] += $blocksToTravel;
                break;
            case 'E':
                $this->coordFinal[0] += $blocksToTravel;
                break;
            case 'S':
                $this->coordFinal[1] -= $blocksToTravel;
                break;
            default:
                $this->coordFinal[0] -= $blocksToTravel;
        }
    }

    protected function travelByIntersection(int $blocksToTravel) : void
    {
        [$x, $y] = $this->coordFinal;

        while ($blocksToTravel-- > 0 && $this->isCollisionFree) {
            switch ($this->cardinalDirection) {
                case 'N':
                    $y++;
                    break;
                case 'E':
                    $x++;
                    break;
                case 'S':
                    $y--;
                    break;
                default:
                    $x--;
            }

            $this->coordFinal = [$x, $y];

            if (in_array($this->coordFinal, $this->intersections)) {
                $this->isCollisionFree = false;
            }
            $this->intersections[] = $this->coordFinal;
        }
    }
}
