<?php
declare(strict_types=1);

namespace Aoc2016\Day03;

use Haystack\HArray;
use Haystack\HString;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

class TriangleThingy
{
    /** @var HArray */
    protected $rawTriangleCandidates;

    public function __construct()
    {
        $this->rawTriangleCandidates = new HArray();
    }

    public function getMax(string $numbers) : string
    {
        return max((new HString($numbers))
            ->toHArray(" ")
            ->toArray()
        );
    }

    public function getMinAndMiddle(string $numbers) : array
    {
        $numArr = (new HString($numbers))->toHArray(" ");
        return $numArr
            ->remove($this->getMax($numbers))
            ->toArray();
    }

    public function getRawTriangleCandidates() : array
    {
        return $this->rawTriangleCandidates->toArray();
    }

    public function getFilteredCandidates() : array
    {
        return $this->rawTriangleCandidates
            ->filter(function ($numbers) {
                return $this->isTriangleCandidate($numbers);
            })
            ->toArray();
    }

    public function isTriangleCandidate(string $numbers) : bool
    {
        $notMax = $this->getMinAndMiddle($numbers);
        $left = (int) $notMax[0] + (int) $notMax[1];

        return $left > (int) $this->getMax($numbers);
    }

    public function read() : void
    {
        $filesystem = new Filesystem(new Local(__DIR__));

        $this->rawTriangleCandidates = (
            new HString($filesystem->read("input.txt"))
        )
            ->toHArray("\n")
            ->filter(function ($lines) {
                return !empty($lines);
            })
            ->map(function($lines) {
                return $this->trimNumbers($lines);
            });
    }

    protected function trimNumbers(string $numbers) : string
    {
        return (new HString($numbers))
            ->toHArray(" ")
            ->filter(function ($item) {
                return !empty($item);
            })
            ->toHString(" ")
            ->toString();
    }
}
